import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";
const endpoint = {
    get_patient: '/patient/findPatientByUserId/',
};

function getPatient(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patient + user.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatient,
};
