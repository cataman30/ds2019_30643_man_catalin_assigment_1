import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../commons/tables/table"
import * as API_USERS from "./api/patientPage-api"
import Cookies from 'universal-cookie';
const cookie = new Cookies();

let columns = [
    {
        Header:  'Address',
        accessor: 'adress',
    },
    {
        Header: 'birthDate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'medicalRecord',
        accessor: 'medicalRecord',
    },
    {
        Header: 'Name',
        accessor: 'name',
    }
];

const filters = [
    {
        accessor: 'adress',
    },
    {
        accessor: 'birthDate',
    },
    {
        accessor: 'gender',
    },
    {
        accessor: 'medicalRecord',
    },
    {
        accessor: 'name',
    },
];



class PatientPage extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {

        let user ={
        id: cookie.get("uid"),
        name: 'not important',
        gender: 'not important',
        adress: 'not important',
        birthDate: '1999-12-12'
    }
        return API_USERS.getPatient(user, (result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.id,
                        adress: x.adress,
                        birthDate: x.birthDate,
                        gender: x.gender,
                        medicalRecord: x.medicalRecord,
                        name: x.name,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
               // this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;

        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default PatientPage;
