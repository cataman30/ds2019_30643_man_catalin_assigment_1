import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientForm from "./patient-form";

import * as API_USERS from "./api/patient-api"
import Button from "react-bootstrap/Button";

function handleDelete(original) {

    let user = {
        id: original.id,
        adress: original.adress,
        birthDate: original.birthDate,
        gender: original.gender,
        medicalRecord: original.medicalRecord,
        name: original.name,
        cg: original.cg,
    };

    console.log("Successfully deleted medication with id: "+original.id);

    return API_USERS.deletePatient(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted medication with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

function handleEdit(original) {
    let user = {
        id: original.id,
        adress: original.adress,
        birthDate: original.birthDate,
        gender: original.gender,
        medicalRecord: original.medicalRecord,
        name: original.name,
        cg: original.cg,
    };

    console.log("Successfully deleted medication with id: "+original.name);

    return API_USERS.updatePatient(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted doctor with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

const filters = [
    {
        accessor: 'adress',
    },
    {
        accessor: 'birthDate',
    },
    {
        accessor: 'gender',
    },
    {
        accessor: 'medicalRecord',
    },
    {
        accessor: 'name',
    },
];

class Patients extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.id,
                        adress: x.adress,
                        birthDate: x.birthDate,
                        gender: x.gender,
                        medicalRecord: x.medicalRecord,
                        name: x.name,
                        cg : x.cg,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;

        let columns = [
            {
                Header:  'Address',
                accessor: 'adress',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.adress} onChange={e => {row.original.adress = e.target.value}}/>
                ),
            },
            {
                Header: 'birthDate',
                accessor: 'birthDate',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.birthDate} onChange={e => {row.original.birthDate = e.target.value}}/>
                ),
            },
            {
                Header: 'Gender',
                accessor: 'gender',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.gender} onChange={e => {row.original.gender = e.target.value}}/>
                ),
            },
            {
                Header: 'medicalRecord',
                accessor: 'medicalRecord',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.medicalRecord} onChange={e => {row.original.medicalRecord = e.target.value}}/>
                ),
            },
            {
                Header: 'Name',
                accessor: 'name',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.name} onChange={e => {row.original.name = e.target.value}}/>
                ),
            },
            {
                Header: 'Controls',
                Cell: row => (
                    <div>
                        <Button onClick={() => handleEdit(row.original)}>Edit</Button>
                        <Button onClick={() => handleDelete(row.original)}>Delete</Button>
                    </div>
                )
            }
        ];

        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PatientForm registerPatient={this.refresh}>

                                </PatientForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Patients;
