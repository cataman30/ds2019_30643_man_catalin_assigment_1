import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/persons">Persons</NavLink>
                            <NavLink href="/doctors">Doctors</NavLink>
                            <NavLink href="/caregivers">Caregivers</NavLink>
                            <NavLink href="/patients">Patients</NavLink>
                            <NavLink href="/medications">Medications</NavLink>
                            <NavLink href="/users">Login</NavLink>
                            <NavLink href="/caregiverPage">Caregiver Page</NavLink>
                            <NavLink href="/patientPage">Patient Page</NavLink>
                        </DropdownItem>


                    </DropdownMenu>
                </UncontrolledDropdown>

            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
