import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Doctors from "./doctor-data/doctor/doctors";
import Caregivers from "./caregiver-data/caregiver/caregivers";
import Patients from "./patient-data/patient/patients";
import Medications from "./medication-data/medication/medications";
import UserForm from "./user-data/user/user-form";
import Redirect from "react-router-dom/Redirect";
import Cookies from 'universal-cookie';
import CaregiverPage from "./caregiverPage/caregiverPage";
import PatientPage from "./patientPage/patientPage";
let enums = require('./commons/constants/enums');
const cookie = new Cookies();
class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/doctors'
                            render={props => (
                            (cookie.get('role')==='Doctor' && cookie.get('loggedIn')==='authenticated')?
                                <Doctors  /> :
                                <Redirect to='/caregivers' />
                        )}
                        />

                        <Route
                            exact
                            path='/caregivers'
                            render={props => (
                                ((cookie.get('role')==='Doctor') && cookie.get('loggedIn')==='authenticated')?
                                    <Caregivers  /> :
                                    <Redirect to='/users' />
                            )}
                        />

                        <Route
                            exact
                            path='/patients'
                            render={props => (
                                ((cookie.get('role')==='Doctor') && cookie.get('loggedIn')==='authenticated')?
                                    <Patients  /> :
                                    <Redirect to='/users' />
                            )}
                        />

                        <Route
                            exact
                            path='/medications'
                            render={props => (
                                ((cookie.get('role')==='Doctor') && cookie.get('loggedIn')==='authenticated')?
                                    <Medications  /> :
                                    <Redirect to='/users' />
                            )}
                        />

                        <Route
                            exact
                            path='/users'
                            render={() => <UserForm/>}
                        />

                        <Route
                            exact
                            path='/caregiverPage'
                            render={props => (
                                ((cookie.get('role')==='Caregiver') && cookie.get('loggedIn')==='authenticated')?
                                    <CaregiverPage  /> :
                                    <Redirect to='/users' />
                            )}
                        />

                        <Route
                            exact
                            path='/patientPage'
                            render={props => (
                                ((cookie.get('role')==='Patient') && cookie.get('loggedIn')==='authenticated')?
                                    <PatientPage  /> :
                                    <Redirect to='/users' />
                            )}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
