import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";
const endpoint = {
    get_patients: '/caregiver/findAllPatients/',
};

function getPatients(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + user.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
};
