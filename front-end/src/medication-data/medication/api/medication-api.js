import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_medications: '/medication/',
    post_medication: "/medication/",
    delete_medication: '/medication/',
    update_medication: '/medication/',
};

function deleteMedication(user, callback)
{
    let request = new Request(HOST.backend_api + endpoint.delete_medication , {
        method: 'DELETE',
        headers : {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(user, callback)
{
    let request = new Request(HOST.backend_api + endpoint.update_medication , {
        method: 'PUT',
        headers : {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_medications + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    getMedicationById,
    postMedication,
    deleteMedication,
    updateMedication
};
