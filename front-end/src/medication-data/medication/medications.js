import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import MedicationForm from "./medication-form";

import * as API_USERS from "./api/medication-api"
import Button from "react-bootstrap/Button";

const filters = [
    {
        accessor: 'name',
    },
    {
        accessor: 'sideEffects',
    },
    {
        accessor: 'dosage',
    },
];

function handleDelete(original) {

    let user = {
        id: original.id,
        name: original.name,
        sideEffects: original.sideEffects,
        dosage: original.dosage,
    };

    console.log("Successfully deleted medication with id: "+original.id);

    return API_USERS.deleteMedication(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted medication with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

function handleEdit(original) {
    let user = {
        id: original.id,
        name: original.name,
        sideEffects: original.sideEffects,
        dosage: original.dosage,
    };

    console.log("Successfully deleted medication with id: "+original.name);

    return API_USERS.updateMedication(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted doctor with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

class Medications extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        sideEffects: x.sideEffects,
                        dosage: x.dosage,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        let columns = [
            {
                Header:  'Name',
                accessor: 'name',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.name} onChange={e => {row.original.name = e.target.value}}/>
                ),
            },
            {
                Header: 'Side Effects',
                accessor: 'sideEffects',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.sideEffects} onChange={e => {row.original.sideEffects = e.target.value}}/>
                ),
            },
            {
                Header: 'Dosage',
                accessor: 'dosage',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.dosage} onChange={e => {row.original.dosage = e.target.value}}/>
                ),
            },
            {
                Header: 'Controls',
                Cell: row => (
                    <div>
                        <Button onClick={() => handleEdit(row.original)}>Edit</Button>
                        <Button onClick={() => handleDelete(row.original)}>Delete</Button>
                    </div>
                )
            }
        ];
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationForm registerMedication={this.refresh}>

                                </MedicationForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Medications;
