import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_doctors: '/doctor/',
    post_doctor: "/doctor",
    delete_doctor: '/doctor/',
    update_doctor: '/doctor/',
};

function deleteDoctor(user, callback)
{
    let request = new Request(HOST.backend_api + endpoint.delete_doctor , {
        method: 'DELETE',
        headers : {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log(JSON.stringify(user));
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateDoctor(user, callback)
{
    let request = new Request(HOST.backend_api + endpoint.update_doctor , {
        method: 'PUT',
        headers : {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getDoctors(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_doctors, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDoctorById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_doctors + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDoctor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_doctor , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getDoctors,
    getDoctorById,
    postDoctor,
    deleteDoctor,
    updateDoctor
};
