import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import DoctorForm from "./doctor-form";
import Button from "react-bootstrap/Button";
import cellEditFactory from 'react-bootstrap-table2-editor';

import * as API_USERS from "./api/doctor-api"
import TextInput from "../../caregiver-data/caregiver/fields/TextInput";

function handleDelete(original) {
    console.log(original);
    let user = {
        id: original.id,
        name: original.name,
        user: original.user
    };

    console.log("Successfully deleted doctor with id: "+original.id);

    return API_USERS.deleteDoctor(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted doctor with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        //window.location.reload();
    });
}

function handleEdit(original) {
    let user = {
        id: original.id,
        name: original.name,
    };

    console.log("Successfully deleted doctor with id: "+original.name);

    return API_USERS.updateDoctor(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted doctor with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

const filters = [
    {
        accessor: 'name',
    }
];

class Doctors extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchDoctors();
    }
    
    fetchDoctors() {
        return API_USERS.getDoctors((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        user: x.user,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        let columns = [
            {
                Header:  'Name',
                accessor: 'name',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.name} onChange={e => {row.original.name = e.target.value}}/>
                ),
            },
            {
                Header: 'Controls',
                Cell: row => (
                    <div>
                        <Button onClick={() => handleEdit(row.original)}>Edit</Button>
                        <Button onClick={() => handleDelete(row.original)}>Delete</Button>
                    </div>
                )
            }
        ];
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <DoctorForm registerDoctor={this.refresh}>

                                </DoctorForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Doctors;
