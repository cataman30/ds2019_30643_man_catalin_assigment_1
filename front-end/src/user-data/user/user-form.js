import React from 'react';
import validate from "./validators/user-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/user-api";
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom'
const cookie = new Cookies();
class UserForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            hidden: true,
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {

                username: {
                    value: '',
                    placeholder: 'Type in the user name...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Type in the password',
                    valid: true,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    state = {
        redirect: false
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            switch (cookie.get('role')) {
                case "Doctor": return <Redirect to='/doctors' />;
                case "Caregiver": return <Redirect to='/caregiverPage' />;
                case "Patient": return <Redirect to='/patientPage' />;
            }
            return <Redirect to='/users' />
        }
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        //updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    handleSubmit(event){
        console.log("Username: " + this.state.formControls.username.value);

        let user = {
            username: this.state.formControls.username.value,
            password : this.state.formControls.password.value,
        };

        API_USERS.getUsers((result, status, err) => {
                console.log(result);
                cookie.set("loggedIn", "nonauth");
                cookie.set("username", "nonauth");
                cookie.set("role", "nonauth");
                cookie.set("uid", "nonauth");
                if(result !== null) {
                    { result.forEach( x => {
                        if(user.username === x.username)
                            if(user.password === x.password)
                            {
                                cookie.set("username", x.username);
                                cookie.set("role", x.role);
                                cookie.set("loggedIn", "authenticated");
                                cookie.set("uid", x.id);
                                this.setRedirect();
                            }
                    });
                    }
                } else {
                    this.state.errorStatus = status;
                    this.state.error = err;
                }
            });
        event.preventDefault();
    }

    render() {
        if(cookie.get('loggedIn') === "authenticated" && this.state.redirect)
            return this.renderRedirect();
            else
        return (
            <form onSubmit={this.handleSubmit}>

                <h1>Log In</h1>

                <p> Name: </p>

                <TextInput name="username"
                           placeholder={this.state.formControls.username.placeholder}
                           value={this.state.formControls.username.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.username.touched}
                           valid={this.state.formControls.username.valid}
                />

                <p> Password: </p>

                <TextInput name="password"
                           type="password"
                           secureTextEntry={true}
                           placeholder={this.state.formControls.password.placeholder}
                           value={this.state.formControls.password.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.password.touched}
                           valid={this.state.formControls.password.valid}
                />

                <p></p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Submit
                </Button>
            </form>

        );
    }
}

export default UserForm;
