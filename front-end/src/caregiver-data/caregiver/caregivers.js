import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import CaregiverForm from "./caregiver-form";

import * as API_USERS from "./api/caregiver-api"
import Button from "react-bootstrap/Button";

const filters = [
    {
        accessor: 'name',
    },
    {
        accessor: 'email',
    },
    {
        accessor: 'birthDate',
    },
    {
        accessor: 'adress',
    }
];

function handleDelete(original) {

    let user = {
        id: original.id,
        adress: original.adress,
        birthDate: original.birthDate,
        gender: original.gender,
        name: original.name,
    };

    console.log("Successfully deleted medication with id: "+original.id);

    return API_USERS.deleteCaregiver(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted medication with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

function handleEdit(original) {
    let user = {
        id: original.id,
        adress: original.adress,
        birthDate: original.birthDate,
        gender: original.gender,
        name: original.name,
    };

    console.log("Successfully deleted medication with id: "+original.name);

    return API_USERS.updateCaregiver(user, (result, status, error) => {
        console.log(result);

        if(result !== null && (status === 200 || status ===201)){
            console.log("Successfully deleted doctor with id: " + result);
        } else {
            this.state.errorStatus = status;
            this.error = error;
        }
        window.location.reload();
    });
}

class Caregivers extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        adress: x.adress,
                        gender: x.gender,
                        birthDate: x.birthDate
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        let columns = [
            {
                Header:  'Address',
                accessor: 'adress',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.adress} onChange={e => {row.original.adress = e.target.value}}/>
                ),
            },
            {
                Header: 'birthDate',
                accessor: 'birthDate',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.birthDate} onChange={e => {row.original.birthDate = e.target.value}}/>
                ),
            },
            {
                Header: 'Gender',
                accessor: 'gender',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.gender} onChange={e => {row.original.gender = e.target.value}}/>
                ),
            },
            {
                Header: 'Name',
                accessor: 'name',
                Cell: row => (
                    <input type={"text"} defaultValue={row.original.name} onChange={e => {row.original.name = e.target.value}}/>
                ),
            },
            {
                Header: 'Controls',
                Cell: row => (
                    <div>
                        <Button onClick={() => handleEdit(row.original)}>Edit</Button>
                        <Button onClick={() => handleDelete(row.original)}>Delete</Button>
                    </div>
                )
            }
        ];
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <CaregiverForm registerCaregiver={this.refresh}>

                                </CaregiverForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Caregivers;
