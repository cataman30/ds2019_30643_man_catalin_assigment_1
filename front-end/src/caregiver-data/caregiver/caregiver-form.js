import React from 'react';
import validate from "./validators/caregiver-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class CaregiverForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {

                adress: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: true,
                    touched: false,

                },
                birthDate: {
                    value: '',
                    placeholder: 'YYYY-MM-DD',
                    valid: true,
                    touched: false,

                },
                gender: {
                    value: '',
                    placeholder: 'M/F',
                    valid: true,
                    touched: false,
                },
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        //updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerCaregiver(caregiver){
        return API_USERS.postCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted caregiver with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }



    handleSubmit(){

        console.log("New caregiver data:");
        console.log("Adress: " + this.state.formControls.adress.value);
        console.log("BirthDate: " + this.state.formControls.birthDate.value);
        console.log("Gender: " + this.state.formControls.gender.value);
        console.log("Name: " + this.state.formControls.name.value);

        let user = {
            adress: this.state.formControls.adress.value,
            birthDate : this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            name: this.state.formControls.name.value,
        };

        this.registerCaregiver(user);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Insert new caregiver</h1>

                <p> Adress: </p>
                <TextInput name="adress"
                           placeholder={this.state.formControls.adress.placeholder}
                           value={this.state.formControls.adress.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.adress.touched}
                           valid={this.state.formControls.adress.valid}
                />

                <p> BirthDate: </p>
                <TextInput name="birthDate"
                           placeholder={this.state.formControls.birthDate.placeholder}
                           value={this.state.formControls.birthDate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.birthDate.touched}
                           valid={this.state.formControls.birthDate.valid}
                />

                <p> Gender: </p>
                <TextInput name="gender"
                           placeholder={this.state.formControls.gender.placeholder}
                           value={this.state.formControls.gender.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.gender.touched}
                           valid={this.state.formControls.gender.valid}
                />

                <p> Name: </p>

                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p></p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Submit
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default CaregiverForm;
