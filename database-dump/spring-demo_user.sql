-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: spring-demo
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  `username` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'DoctorPass','Doctor','A5FG78MCKF'),(2,'DoctorPass','Doctor','CBUD5OAAJS'),(3,'DoctorPass','Doctor','W5UWN1DO0C'),(4,'DoctorPass','Doctor','1MYIMG0J6C'),(5,'DoctorPass','Doctor','QKYAP2ZEN9'),(6,'DoctorPass','Doctor','WYZMOSUC04'),(7,'DoctorPass','Doctor','GZJPVTAVQC'),(8,'DoctorPass','Doctor','ZR7UQLBYTD'),(9,'DoctorPass','Doctor','HD112FQTJ8'),(10,'DoctorPass','Doctor','CB4IU0DEN3'),(11,'DoctorPass','Doctor','9SXOL6ALXW'),(12,'DoctorPass','Doctor','MGNE7U3ID4'),(13,'DoctorPass','Doctor','RNNRNCQKZ6'),(14,'DoctorPass','Doctor','C13DXSMGYE'),(15,'DoctorPass','Doctor','NMU149XZSO'),(16,'CaregiverPass','Caregiver','TestCaregiver'),(17,'DoctorPass','Doctor','L2CW4YZR1E'),(18,'PatientPass','Patient','5HZQC4EJV1'),(19,'CaregiverPass','Caregiver','C9QEV0LV4X'),(20,'DoctorPass','Doctor','M477PEVSWY'),(23,'DoctorPass','Doctor','NUWFJ3M6L5'),(24,'CaregiverPass','Caregiver','EWD1IM8JZE'),(25,'CaregiverPass','Caregiver','LKFDYE9O4T'),(28,'CaregiverPass','Caregiver','WP2KGSTAVM'),(29,'PatientPass','Patient','JESTNGHP9N'),(30,'PatientPass','Patient','TLCA5P7D8J');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-25  8:43:45
